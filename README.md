## 微服务的客户端SDK

## 使用方式

在composer.json里添加如下内容

```json

{
	"require":{
		"JulianJiang/huosdk":"dev-master"
	},
	"repositories":[        
		{
		    "type":"git",
			"url":"https://git.oschina.net/julianjiang/sdk.git"			
		}
    ]
}

```

然后执行composer install

## 已经完成的接口

1. 支付宝提现申请