<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/26
 * Time: 11:13
 */
error_reporting(E_ALL);

require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/Exception/HuoException.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/Request/IRequest.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/HuoClient.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/ResultSet.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/Utility/RequestCheckUtility.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/Utility/RSASign.php');
require realpath(dirname(dirname(__FILE__)) . '/src/HuoSdk/Request/Finance/FundAlipayRequest.php');

$client = new \HuoSdk\HuoClient();

$request = new \HuoSdk\Request\Finance\FundAlipayRequest();
$request->setAmount(0.1);
$request->setPayeeAccount("18106586291");
$request->setPayeeRealName("江俊");
$request->setUserid(1);
$result = $client->execute($request);
var_dump($result);
