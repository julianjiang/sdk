<?php
/**
 * @package HuoSdk
 *
 * @internal 异常类
 *
 * @author jiangjun
 * @date 2017/2/23 10:57
 * @version
 */
namespace HuoSdk\Exception;

/**
 * Class HuoException
 * @package HuoSdk\Exception
 */
class HuoException extends \Exception
{

}