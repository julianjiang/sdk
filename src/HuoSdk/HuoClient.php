<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/26
 * Time: 9:32
 */

namespace HuoSdk;

use HuoSdk\Exception\HuoException;
use HuoSdk\Utility\RSASign;
class HuoClient
{
    public $appkey;

    public $secretKey;

    public $gatewayUrl = "http://192.168.10.6:8080/api/public/";

    public $format = "json";

    public $connectTimeout;

    public $readTimeout;

    /** 是否打开入参check**/
    public $checkRequest = true;

    protected $signMethod = "RSA";

    protected $apiVersion = "1.0";

    protected $sdkVersion = "huo-sdk-php-20170426";

    public function __construct($appkey = "",$secretKey = ""){
        $this->appkey = $appkey;
        $this->secretKey = $secretKey ;
    }

    protected function generateSign(&$params)
    {
        ksort($params);
        foreach ($params as $key => $item) {
            if(!is_array($item) && "@" != substr($item, 0, 1)) {
                $params[$key] = (string)$item;
            }
        }

        $params_json = json_encode($params);
        $params_urlencode = urlencode($params_json);
        $params_md5 = md5($params_urlencode);
        $class = new RSASign();
        $sign = $class->signWithPri($params_md5);
        return $sign;
    }


    public function execute($request)
    {
        $result =  new ResultSet();
        if($this->checkRequest) {
            try {
                $request->check();
            } catch (Exception $e) {

                $result->errorCode = $e->getCode();
                $result->errorDescription = $e->getMessage();
                return $result;
            }
        }
        //组装系统参数
        $sysParams["app_key"] = $this->appkey;
        $sysParams["secret_key"] = $this->secretKey;
        $sysParams["v"] = $this->apiVersion;
        $sysParams["format"] = $this->format;
        $sysParams["sign_method"] = $this->signMethod;
        $sysParams["timestamp"] = date("Y-m-d H:i:s");
        $sysParams["partner_id"] = $this->sdkVersion;
        $huoParams = [];
        //获取业务参数
        $huoParams = $request->getHuoParas();


        //系统参数放入GET请求串
        $method = $request->getApiMethodName();
        $requestUrl = $this->gatewayUrl.$method;


        $postFields = array_merge($huoParams, $sysParams);
        //签名
        $postFields["sign"] = $this->generateSign($postFields);



        //发起HTTP请求
        try
        {
            $resp = $this->curl($requestUrl, $postFields);
        }
        catch (Exception $e)
        {
            $result->errorCode = $e->getCode();
            $result->errorDescription = $e->getMessage();
            return $result;
        }

        unset($apiParams);
        unset($fileFields);
        //解析TOP返回结果
        $respWellFormed = false;
        if ("json" == $this->format)
        {
            $respObject = json_decode($resp);
            if (null !== $respObject)
            {
                $respWellFormed = true;
            }
        }
        else if("xml" == $this->format)
        {
            $respObject = @simplexml_load_string($resp);
            if (false !== $respObject)
            {
                $respWellFormed = true;
            }
        }

        //返回的HTTP文本不是标准JSON或者XML，记下错误日志
        if (false === $respWellFormed)
        {
            $result->errorCode = 0;
            $result->errorDescription = "HTTP_RESPONSE_NOT_WELL_FORMED";
            return $result;
        }

        return $respObject;
    }


    public function curl($url, $postFields = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($this->readTimeout) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->readTimeout);
        }
        if ($this->connectTimeout) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        }
        curl_setopt ( $ch, CURLOPT_USERAGENT, "huo-sdk-php" );
        //https 请求
        if(strlen($url) > 5 && strtolower(substr($url,0,5)) == "https" ) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        if (is_array($postFields) && 0 < count($postFields))
        {
            $postMultipart = false;
            foreach ($postFields as $k => $v)
            {
                if("@" != substr($v, 0, 1))//判断是不是文件上传
                {

                }
                else//文件上传用multipart/form-data，否则用www-form-urlencoded
                {
                    $postMultipart = true;
                    if(class_exists('\CURLFile')){
                        $postFields[$k] = new \CURLFile(substr($v, 1));
                    }
                }
            }
            unset($k, $v);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($postMultipart)
            {
                if (class_exists('\CURLFile')) {
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
                } else {
                    if (defined('CURLOPT_SAFE_UPLOAD')) {
                        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                    }
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
            else
            {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
        }
        $reponse = curl_exec($ch);
        if (curl_errno($ch))
        {
            throw new HuoException(curl_error($ch),0);
        }
        else
        {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode)
            {
                throw new HuoException($reponse,$httpStatusCode);
            }
        }
        curl_close($ch);
        return $reponse;
    }

}