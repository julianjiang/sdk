<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/26
 * Time: 10:54
 */

namespace HuoSdk\Request\Finance;

use HuoSdk\Exception\HuoException;
use HuoSdk\Request\IRequest;
use HuoSdk\Utility\RequestCheckUtility;


/**
 * 支付宝提现请求类
 * Class FundAlipayRequest
 * @package HuoSdk\Request\Finance
 */
class FundAlipayRequest implements IRequest
{

    /**
     * 提现人支付宝账号
     * @var
     */
    private $payee_account;

    /**
     * 提现金额
     * @var
     */
    private $amount;

    /**
     * 提现人用户id
     * @var
     */
    private $userid;

    /**
     * 提现人支付宝真实姓名
     * @var
     */
    private $payee_real_name;

    /**
     * 提现方式，默认1
     * @var int
     */
    private $paytype = 1;


    private $huoParas = array();


    public function getPayeeAccount()
    {
        return $this->payee_account;
    }

    public function setPayeeAccount($payee_account){
        $this->payee_account = $payee_account;
        $this->huoParas['payee_account'] = $payee_account;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount){
        $this->amount = $amount;
        $this->huoParas['amount'] = $amount;
    }


    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid($userid){
        $this->userid = $userid;
        $this->huoParas['userid'] = $userid;
    }


    public function getPayeeRealName()
    {
        return $this->payee_real_name;
    }

    public function setPayeeRealName($payee_real_name){
        $this->payee_real_name = $payee_real_name;
        $this->huoParas['payee_real_name'] = $payee_real_name;
    }


    public function getApiMethodName()
    {
        return "finance-fund-alipay";
    }

    public function getHuoParas()
    {
        $this->huoParas['paytype'] = $this->paytype;
        return $this->huoParas;
    }

    public function check()
    {
        RequestCheckUtility::checkNotNull($this->payee_account,"payee_account");
        RequestCheckUtility::checkNotNull($this->payee_real_name,"payee_real_name");
        RequestCheckUtility::checkNotNull($this->amount,"amount");
        RequestCheckUtility::checkNotNull($this->userid,"userid");

        RequestCheckUtility::checkNumeric($this->amount,"amount");
        RequestCheckUtility::checkNumeric($this->userid,"amount");
    }

    public function putOtherTextParam($key, $value) {
        $this->huoParas[$key] = $value;
        $this->$key = $value;
    }

    public function fillParam($params=[]){
        if(!is_array($params)){
            throw new HuoException("fill-params-error:params must be array");
        }
        $this->huoParas = $params;
    }
}