<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/26
 * Time: 10:59
 */

namespace HuoSdk\Request;


/**
 * 请求接口
 * Interface IRequest
 * @package HuoSdk\Request
 */
interface IRequest
{

    /**
     * 请求所需要的业务参数
     * @return mixed
     */
    public function getHuoParas();


    /**
     * 发送请求前的参数检查
     * @return mixed
     */
    public function check();


    /**
     * 设置文本参数
     * @param $key
     * @param $value
     * @return mixed
     */
    public function putOtherTextParam($key, $value);


    /**
     * 批量设置参数
     * @param $params
     * @return mixed
     */
    public function fillParam($params);
}