<?php
namespace HuoSdk;
/**
 * 返回的默认类
 * 
 * @author jiangjun create
 * @since 1.0, 2015-01-20
 */
class ResultSet
{
	
	/** 
	 * 返回的错误码
	 **/
	public $errorCode;
	
	/** 
	 * 返回的错误信息
	 **/
	public $errorDescription;
	
}
